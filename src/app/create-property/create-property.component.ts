import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from "@angular/forms";

@Component({
  selector: 'app-create-property',
  templateUrl: './create-property.component.html',
  styleUrls: ['./create-property.component.css']
})
export class CreatePropertyComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder
  ) { }

  propertyImages = [];
  propertyDocuments = [null, null];
  property;
  ngOnInit(): void {
    this.property = this.formBuilder.group({
      owner_name: new FormControl(""),
      owner_id: new FormControl(""),
      owner_id_number: new FormControl(""),
      document1_id: new FormControl(""),
      document2_id: new FormControl(""),
    })
  }

  readURL(event) {
    var input = event.target;
    if (input.files[0]) {
      var reader = new FileReader();
      reader.onload = (e: any) => {
        this.propertyImages.push(e.target.result);
      };
      reader.readAsDataURL(input.files[0]);
    }
  }

  removeImage(i){
    this.propertyImages.splice(i, 1);
  }

  uploadFile(input, index){
    if (input.files[0]) {
      var reader = new FileReader();
      reader.onload = (e: any) => {
        this.propertyDocuments[index] = (e.target.result);
      };
      reader.readAsDataURL(input.files[0]);
    }
  }
  createProperty(){
    this.property.value["propertyImages"] = this.propertyImages;
    this.property.value["propertyDocuments"] = this.propertyDocuments;
    console.log(this.property.value);
  }
}
