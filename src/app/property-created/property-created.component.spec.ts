import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropertyCreatedComponent } from './property-created.component';

describe('PropertyCreatedComponent', () => {
  let component: PropertyCreatedComponent;
  let fixture: ComponentFixture<PropertyCreatedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropertyCreatedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropertyCreatedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
