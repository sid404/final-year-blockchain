import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-search-property',
  templateUrl: './search-property.component.html',
  styleUrls: ['./search-property.component.css']
})
export class SearchPropertyComponent implements OnInit {

  constructor() { }
  loading:boolean = true;
  searchTerm=null;
  public searchProperty(){
    this.loading = !this.loading;
  }
  ngOnInit(): void {

  }

}
