import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-authority',
  templateUrl: './authority.component.html',
  styleUrls: ['./authority.component.css']
})
export class AuthorityComponent implements OnInit {

  constructor() { }

  requests = [
    {
      id: "788465132",
      date: "03-02-2020",
      approved: false,
      rejected: false
    },
    {
      id: "456789130",
      date: "29-08-2019",
      approved: false,
      rejected: false
    },
    {
      id: "4251369807",
      date: "15-07-2019",
      approved: false,
      rejected: false
    },
    {
      id: "2134567890",
      date: "20-05-2019",
      approved: false,
      rejected: false
    }
  ]
  selectedTransaction;
  sellerDocuments = [
    {
      document_type: "Aadhar",
      approved: false,
      rejected: false
    },
    {
      document_type: "PAN",
      approved: false,
      rejected: false
    },
    {
      document_type: "Registry",
      approved: false,
      rejected: false
    },
    {
      document_type: "PR Card",
      approved: false,
      rejected: false
    }
  ]
  buyerDocuments  = [
    {
      document_type: "Aadhar",
      approved: false,
      rejected: false
    },
    {
      document_type: "PAN",
      approved: false,
      rejected: false
    },
    // {
    //   document_type: "",
    //   approved: false,
    //   rejected: false
    // },
    // {
    //   document_type: "",
    //   approved: false,
    //   rejected: false
    // }
  ]
  transactionStatus = 0;
  ngOnInit(): void {
    console.log(this.requests);
    
  }
  getTransactionDetails(request){
    this.selectedTransaction = true
    console.log(request);
  }
  acceptDocument(index, flag){
    if(flag == 0){
      this.sellerDocuments[index]["approved"] = true
    } else {
      this.buyerDocuments[index]["approved"] = true
    }
  }
  rejectDocument(index, flag){
    if(flag == 0){
      this.sellerDocuments[index]["rejected"] = true
    } else {
      this.buyerDocuments[index]["rejected"] = true
    }
  }
  changeTransactionStatus(value){
    this.transactionStatus = value
  }

}
