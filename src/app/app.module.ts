import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponentComponent } from './home-component/home-component.component';
import { NavbarComponent } from './navbar/navbar.component';
import { PropertyDetailsComponent } from './property-details/property-details.component';
import { AuthorityComponent } from './authority/authority.component';
import { CreatePropertyComponent } from './create-property/create-property.component';

import { SlickCarouselModule } from 'ngx-slick-carousel';
import { PropertyCreatedComponent } from './property-created/property-created.component';
import { SearchPropertyComponent } from './search-property/search-property.component';
import { BuyerVerificationComponent } from './buyer-verification/buyer-verification.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponentComponent,
    NavbarComponent,
    PropertyDetailsComponent,
    PropertyCreatedComponent,
    SearchPropertyComponent,
    AuthorityComponent,
    CreatePropertyComponent,
    BuyerVerificationComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SlickCarouselModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
