import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-property-details',
  templateUrl: './property-details.component.html',
  styleUrls: ['./property-details.component.css']
})
export class PropertyDetailsComponent implements OnInit {

  constructor() { }

  slides = [342, 453, 846, 855, 234, 564, 744, 243];
 
  slideConfig = {
    "slidesToShow": 1,
    "slidesToScroll": 1, 
    "nextArrow": "<div class='nav-btn next-slide'></div>",
    "prevArrow": "<div class='nav-btn prev-slide'></div>",
    "dots": true,
    "infinite": false
  };

  carouselConfig = {
    "slidesToShow": 3,
    "slidesToScroll": 1, 
    "nextArrow": "<div class='nav-btn next-slide'></div>",
    "prevArrow": "<div class='nav-btn prev-slide'></div>",
    "dots": false,
    "infinite": true
  };
  
  // addSlide() {
  //   this.slides.push(488)
  // }
 
  // removeSlide() {
  //   this.slides.length = this.slides.length - 1;
  // }
 
  slickInit(e) {
    console.log('slick initialized');
  }
 
  breakpoint(e) {
    console.log('breakpoint');
  }
 
  afterChange(e) {
    console.log('afterChange');
  }
 
  beforeChange(e) {
    console.log('beforeChange');
  }

  ngOnInit(): void {
  }



}
