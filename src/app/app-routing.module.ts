import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponentComponent } from './home-component/home-component.component';
import { PropertyDetailsComponent } from './property-details/property-details.component';
import { PropertyCreatedComponent } from './property-created/property-created.component';
import { SearchPropertyComponent } from './search-property/search-property.component';
import { AuthorityComponent } from './authority/authority.component';
import { CreatePropertyComponent } from './create-property/create-property.component';
import { BuyerVerificationComponent } from './buyer-verification/buyer-verification.component';

const routes: Routes = [
  { path: '', component: HomeComponentComponent },
  { path: 'property/:id', component: PropertyDetailsComponent },
  { path: "authority", component: AuthorityComponent },
  { path: "create-property", component: CreatePropertyComponent },
  { path: "upload-documents", component: BuyerVerificationComponent },
  {path:'property-created',component:PropertyCreatedComponent},
  {path:'search-property', component:SearchPropertyComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
