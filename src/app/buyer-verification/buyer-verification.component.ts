import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-buyer-verification',
  templateUrl: './buyer-verification.component.html',
  styleUrls: ['./buyer-verification.component.css']
})
export class BuyerVerificationComponent implements OnInit {

  constructor() { }

  aadhar;
  pan;
  ngOnInit(): void {
  }
  uploadFile(input, flag){
    if (input.files[0]) {
      var reader = new FileReader();
      reader.onload = (e: any) => {
        if(flag == 0){
          this.aadhar = (e.target.result);
        } else{
          this.pan = (e.target.result);
        }
      };
      reader.readAsDataURL(input.files[0]);
    }
  }
  uploadDocuments(){
    console.log(this.aadhar, this.pan);
    
  }
}
