import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyerVerificationComponent } from './buyer-verification.component';

describe('BuyerVerificationComponent', () => {
  let component: BuyerVerificationComponent;
  let fixture: ComponentFixture<BuyerVerificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyerVerificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyerVerificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
